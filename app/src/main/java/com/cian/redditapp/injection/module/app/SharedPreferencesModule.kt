package com.cian.redditapp.injection.module.app

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.cian.domain.repository.SharedPreferencesRepository
import com.cian.data.repository.SharedPreferencesRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class SharedPreferencesModule {

    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    fun providePreferencesRepository(
        sharedPreferences: SharedPreferences
    ): SharedPreferencesRepository =
        SharedPreferencesRepositoryImpl(sharedPreferences)
}

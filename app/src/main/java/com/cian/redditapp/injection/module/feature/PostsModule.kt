package com.cian.redditapp.injection.module.feature

import com.cian.domain.repository.PostsRepository
import com.cian.data.repository.PostsRepositoryImpl
import com.cian.domain.source.PostsDataSource
import com.cian.data.source.remote.PostsDataSourceImpl
import com.cian.data.source.remote.request.SubredditApi
import com.cian.redditapp.ui.main.posts.PostsViewModel
import com.cian.data.util.ResourceProvider
import com.cian.domain.repository.HistoryRepository
import com.cian.domain.usecase.DeleteAllHistoryUseCase
import com.cian.domain.usecase.GetPostsUseCase
import com.cian.domain.usecase.GetThemeUseCase
import com.cian.domain.usecase.SaveThemeUseCase
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit

@Module
class PostsModule {

    @Provides
    fun provideRedditApi(
        retrofit: Retrofit
    ): SubredditApi = retrofit.create(SubredditApi::class.java)

    @Provides
    fun providePostsDataSource(
        subredditApi: SubredditApi,
        helper: ResourceProvider
    ): PostsDataSource = PostsDataSourceImpl(subredditApi, helper)

    @Provides
    fun providePostsRepository(
        postsDataSource: PostsDataSource
    ): PostsRepository = PostsRepositoryImpl(postsDataSource)

    @Provides
    fun provideGetPostsUseCase(
        postsRepository: PostsRepository
    ): GetPostsUseCase = GetPostsUseCase(postsRepository)

    @Provides
    fun provideDeleteAllHistoryUseCase(
        historyRepository: HistoryRepository
    ): DeleteAllHistoryUseCase = DeleteAllHistoryUseCase(historyRepository)

    @Provides
    fun providePostsViewModel(
        getPostsUseCase: GetPostsUseCase,
        getThemeUseCase: GetThemeUseCase,
        saveThemeUseCase: SaveThemeUseCase,
        deleteAllHistoryUseCase: DeleteAllHistoryUseCase
    ): PostsViewModel =
        PostsViewModel(
            getPostsUseCase,
            getThemeUseCase,
            saveThemeUseCase,
            deleteAllHistoryUseCase,
            Dispatchers.Main
        )
}

@Subcomponent(modules = [PostsModule::class, ThemeModule::class])
interface PostsComponent {
    val postsViewModel: PostsViewModel
}
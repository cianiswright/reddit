package com.cian.redditapp.injection.module.feature

import com.cian.data.repository.SuggestionsRepositoryImpl
import com.cian.data.source.remote.SuggestionsDataSourceImpl
import com.cian.data.source.remote.request.SuggestionApi
import com.cian.data.util.ResourceProvider
import com.cian.domain.repository.HistoryRepository
import com.cian.domain.repository.SuggestionsRepository
import com.cian.domain.source.SuggestionsDataSource
import com.cian.domain.usecase.DeleteHistoryUseCase
import com.cian.domain.usecase.GetHistoryUseCase
import com.cian.domain.usecase.GetSuggestionsUseCase
import com.cian.domain.usecase.SaveHistoryUseCase
import com.cian.redditapp.ui.main.search.SuggestionViewModel
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit

@Module
class SuggestionModule {

    @Provides
    fun provideSuggestionApi(
        retrofit: Retrofit
    ): SuggestionApi = retrofit.create(SuggestionApi::class.java)

    @Provides
    fun provideSuggestionsDataSource(
        suggestionApi: SuggestionApi,
        resourceProvider: ResourceProvider
    ): SuggestionsDataSource = SuggestionsDataSourceImpl(suggestionApi, resourceProvider)

    @Provides
    fun provideSuggestionsRepository(
        suggestionsDataSource: SuggestionsDataSource
    ): SuggestionsRepository = SuggestionsRepositoryImpl(suggestionsDataSource)

    @Provides
    fun provideSuggestionsUseCase(
        suggestionsRepository: SuggestionsRepository
    ): GetSuggestionsUseCase = GetSuggestionsUseCase(suggestionsRepository)

    @Provides
    fun provideGetHistoryUseCase(
        historyRepository: HistoryRepository
    ): GetHistoryUseCase = GetHistoryUseCase(historyRepository)

    @Provides
    fun provideSaveHistoryUseCase(
        historyRepository: HistoryRepository
    ): SaveHistoryUseCase = SaveHistoryUseCase(historyRepository)

    @Provides
    fun provideDeleteHistoryUseCase(
        historyRepository: HistoryRepository
    ): DeleteHistoryUseCase = DeleteHistoryUseCase(historyRepository)

    @Provides
    fun provideSuggestionViewModel(
        getSuggestionsUseCase: GetSuggestionsUseCase,
        getHistoryUseCase: GetHistoryUseCase,
        saveHistoryUseCase: SaveHistoryUseCase,
        deleteHistoryUseCase: DeleteHistoryUseCase
    ): SuggestionViewModel =
        SuggestionViewModel(
            getSuggestionsUseCase,
            getHistoryUseCase,
            saveHistoryUseCase,
            deleteHistoryUseCase,
            Dispatchers.Main
        )
}

@Subcomponent(modules = [SuggestionModule::class])
interface SuggestionComponent {
    val suggestionViewModel: SuggestionViewModel
}
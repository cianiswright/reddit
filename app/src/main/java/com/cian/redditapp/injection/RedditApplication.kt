package com.cian.redditapp.injection

import android.app.Application
import com.cian.redditapp.injection.module.app.AppModule
import com.cian.redditapp.injection.module.app.RoomModule
import com.cian.redditapp.injection.module.app.SharedPreferencesModule
import com.cian.redditapp.injection.component.AppComponent
import com.cian.redditapp.injection.component.DaggerAppComponent

class RedditApplication : Application() {
    companion object {
        lateinit var instance: RedditApplication
    }

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        instance = this

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .roomModule(RoomModule())
            .sharedPreferencesModule(SharedPreferencesModule())
            .build()
    }
}
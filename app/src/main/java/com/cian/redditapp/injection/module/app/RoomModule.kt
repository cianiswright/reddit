package com.cian.redditapp.injection.module.app

import android.content.Context
import com.cian.data.repository.HistoryRepositoryImpl
import com.cian.data.source.local.dao.HistoryDao
import com.cian.data.source.local.db.AppDatabase
import com.cian.domain.repository.HistoryRepository
import dagger.Module
import dagger.Provides

@Module
class RoomModule {

    @Provides
    fun provideAppDatabase(
        context: Context
    ) = AppDatabase.getDb(context)

    @Provides
    fun provideHistoryDao(
        db: AppDatabase
    ) = db.historyDao()

    @Provides
    fun provideHistoryRepository(
        historyDao: HistoryDao
    ): HistoryRepository =
        HistoryRepositoryImpl(historyDao)
}
package com.cian.redditapp.injection.module.feature

import com.cian.domain.repository.SharedPreferencesRepository
import com.cian.domain.usecase.GetThemeUseCase
import com.cian.domain.usecase.SaveThemeUseCase
import com.cian.redditapp.ui.main.MainViewModel
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class ThemeModule {

    @Provides
    fun saveThemeUseCase(
        sharedPreferencesRepository: SharedPreferencesRepository
    ): SaveThemeUseCase = SaveThemeUseCase(sharedPreferencesRepository)

    @Provides
    fun getThemeUseCase(
        sharedPreferencesRepository: SharedPreferencesRepository
    ): GetThemeUseCase = GetThemeUseCase(sharedPreferencesRepository)

    @Provides
    fun provideMainViewModel(
        getThemeUseCase: GetThemeUseCase
    ): MainViewModel =
        MainViewModel(getThemeUseCase)
}

@Subcomponent(modules = [ThemeModule::class])
interface ThemeComponent {
    val mainViewModel: MainViewModel
}
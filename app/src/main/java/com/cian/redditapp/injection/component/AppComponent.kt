package com.cian.redditapp.injection.component

import com.cian.redditapp.injection.module.app.AppModule
import com.cian.redditapp.injection.module.app.NetworkModule
import com.cian.redditapp.injection.module.app.RoomModule
import com.cian.redditapp.injection.module.app.SharedPreferencesModule
import com.cian.redditapp.injection.module.feature.*
import com.cian.redditapp.ui.main.MainActivity
import dagger.Component

@Component(modules = [AppModule::class, RoomModule::class, SharedPreferencesModule::class, NetworkModule::class])
interface AppComponent {
    fun plus(module: PostsModule): PostsComponent
    fun plus(module: SuggestionModule): SuggestionComponent
    fun plus(module: ThemeModule): ThemeComponent

    fun inject(mainActivity: MainActivity)
}



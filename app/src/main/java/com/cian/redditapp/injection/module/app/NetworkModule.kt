package com.cian.redditapp.injection.module.app

import com.cian.data.source.remote.service.NetworkService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class NetworkModule {

    @Provides
    fun provideRetrofit(): Retrofit = NetworkService.getRetrofit()
}
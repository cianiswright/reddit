package com.cian.redditapp.injection.module.app

import android.content.Context
import com.cian.redditapp.injection.RedditApplication
import com.cian.data.util.ResourceProvider
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: RedditApplication) {

    @Provides
    fun provideContext(): Context = application

    @Provides
    fun provideResources(context: Context) = ResourceProvider(context)
}
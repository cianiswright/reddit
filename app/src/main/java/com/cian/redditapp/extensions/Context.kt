package com.cian.redditapp.extensions

import android.content.Context
import com.cian.redditapp.injection.RedditApplication

val Context.app: RedditApplication
    get() = applicationContext as RedditApplication
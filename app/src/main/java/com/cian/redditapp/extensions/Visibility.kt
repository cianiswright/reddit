package com.cian.redditapp.extensions

import android.view.View.VISIBLE
import android.view.View.INVISIBLE
import android.view.View.GONE
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.visible() { this.visibility = VISIBLE }
fun RecyclerView.invisible() { this.visibility = INVISIBLE }
fun RecyclerView.gone() { this.visibility = GONE }

fun TextView.visible() { this.visibility = VISIBLE }
fun TextView.invisible() { this.visibility = GONE }

fun ImageView.visible() { this.visibility = VISIBLE }
fun ImageView.gone() { this.visibility = GONE }
package com.cian.redditapp.extensions

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

fun<T: ViewBinding> Fragment.viewBinding(
    viewBindingFactory: (View) -> T
): ReadOnlyProperty<Fragment, T> = object : ReadOnlyProperty<Fragment, T> {

    private var binding: T? = null

    init {
        /*
        * Observe the view's lifecycle and handle the binding reference
        * in onPause to avoid issues with onDestroyView being skipped.
        * */
        viewLifecycleOwnerLiveData.observe(this@viewBinding, { viewLifecycleOwner ->
            viewLifecycleOwner.lifecycle.addObserver(object: DefaultLifecycleObserver {
                override fun onPause(owner: LifecycleOwner) {
                    (binding as? ViewDataBinding)?.unbind()
                    binding = null
                }
            })
        })

        /*
        * Enforce that views are created consistently. Alternatively, the layout can still be set in the onCreateView().
        * If a usecase exists for the baseFragment to be constructed without a layout, this can be removed.
        * */
        lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onStart(owner: LifecycleOwner) {
                view ?: error("You must pass the layout ID into ${this@viewBinding.javaClass.simpleName}'s constructor")
            }
        })
    }

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        // Check if binding has already been created.
        binding?.let { return it }

        val viewLifecycleOwner = try {
            thisRef.viewLifecycleOwner
        } catch (e: IllegalStateException) {
            error("Don't attempt to get Fragment's bindings when the view hasn't been created.")
        }
        if (!viewLifecycleOwner.lifecycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
            error("Don't attempt to get the Fragment's binding when the view has been destroyed.")
        }

        return viewBindingFactory(thisRef.requireView()).also { viewBinding ->
            // If data binding, then set the lifecycle owner.
            if (viewBinding is ViewDataBinding) {
                viewBinding.lifecycleOwner = viewLifecycleOwner
            }

            this.binding = viewBinding
        }
    }
}
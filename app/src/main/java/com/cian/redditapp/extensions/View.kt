package com.cian.redditapp.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

inline fun View.onClick(crossinline onClick: () -> Unit) =
    setOnClickListener { onClick() }

fun View.hideKeyboard() =
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(windowToken, 0)
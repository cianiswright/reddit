package com.cian.redditapp.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.cian.redditapp.R
import com.google.android.material.bottomsheet.BottomSheetDialog

fun ImageView.loadThumbnail(thumbnail: String) {
    Glide.with(context).load(thumbnail)
        .override(90, 60)
        .centerCrop()
        .into(this)
}

fun ImageView.loadImage(mediaUrl: String) {
    Glide.with(context).load(mediaUrl)
        .placeholder(R.color.mine_shaft)
        .into(this)
}

fun ImageView.loadSuggestionImage(imageUrl: String) {
    if ("https" in imageUrl)
        Glide.with(context).load(imageUrl)
            .override(30, 30)
            .centerCrop()
            .into(this)
}

fun ImageView.updateFilterImageIcon(filter: String) {
    // Get selected filter icon
    var icon = 0
    when (filter) {
        "best" -> icon = R.drawable.ic_filter_best
        "hot" -> icon = R.drawable.ic_filter_hot
        "rising" -> icon = R.drawable.ic_filter_rising
        "card" -> icon = R.drawable.ic_view_card
        "list" -> icon = R.drawable.ic_view_list
    }

    this.setImageResource(icon)
}
package com.cian.redditapp.ui.main.posts.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cian.domain.model.PostItem
import com.cian.domain.util.Constants.CARDVIEW
import com.cian.domain.util.Constants.LISTVIEW
import com.cian.domain.util.Constants.LOADING
import com.cian.redditapp.R
import com.cian.redditapp.databinding.ItemPostCardBinding
import com.cian.redditapp.databinding.ItemPostListBinding
import com.cian.redditapp.extensions.loadThumbnail
import com.cian.redditapp.extensions.loadImage
import com.cian.redditapp.extensions.inflate
import com.cian.redditapp.extensions.visible
import com.cian.redditapp.extensions.gone
import com.cian.redditapp.common.ViewType

class PostsAdapter(
    val listener: (String) -> Unit
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var displayAsCard = true
    private var items: ArrayList<Any> = ArrayList()
    private val loadingItem = object : ViewType { override fun getViewType(): Int = LOADING }

    override fun getItemCount(): Int = items.size

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            itemCount - 1 -> LOADING
            else -> {
                when (displayAsCard) {
                    false -> LISTVIEW
                    true -> CARDVIEW
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            LISTVIEW -> {
                val binding = DataBindingUtil.inflate<ItemPostListBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_post_list, parent, false)
                PostListViewHolder(binding)
            } CARDVIEW -> {
                val binding = DataBindingUtil.inflate<ItemPostCardBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_post_card, parent, false)
                PostCardViewHolder(binding)
            } else -> LoadingViewHolder(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            LISTVIEW -> (holder as PostListViewHolder).bind(items[position] as PostItem)
            CARDVIEW -> (holder as PostCardViewHolder).bind(items[position] as PostItem)
        }
    }

    fun bindPostItem(item: PostItem, mediaType: String, ivMedia: ImageView) {
        when (item.mediaType) {
            "image" -> {
                if (mediaType == "list")
                    ivMedia.loadThumbnail(item.thumbnail!!)
                else ivMedia.loadImage(item.media!!)
                ivMedia.visible()
            }

            else -> ivMedia.gone()
        }
    }

    inner class PostListViewHolder(private val binding: ItemPostListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.setOnClickListener { listener(binding.subItem!!.url) }
        }

        fun bind(item: PostItem) {
            binding.subItem = item
            bindPostItem(item, "list", binding.ivMedia)
        }
    }

    inner class PostCardViewHolder(private val binding: ItemPostCardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.setOnClickListener { listener(binding.subItem!!.url) }
        }

        fun bind(item: PostItem) {
            binding.subItem = item
            bindPostItem(item, "card", binding.ivMedia)
        }
    }

    inner class LoadingViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.item_loading)
    )

    fun addItems(posts: List<PostItem>) {
        val pos = if (items.size != 0) items.size - 1 else items.size
        if (items.size != 0) {
            items.removeAt(pos)
            notifyItemRemoved(pos)
        }

        items.addAll(posts)
        items.add(loadingItem)
        notifyItemRangeInserted(pos, items.size + 1)
    }

    fun clearAndAddItems(posts: List<PostItem>) {
        items.clear()
        notifyDataSetChanged()

        items.addAll(posts)
        items.add(loadingItem)
        notifyItemRangeInserted(0, items.size)
    }

    fun changeViewType(showAsCard: Boolean) {
        displayAsCard = showAsCard
        notifyDataSetChanged()
    }
}
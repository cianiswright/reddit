package com.cian.redditapp.ui.launch

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cian.redditapp.ui.main.MainActivity

class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Show the app icon until the main activity has been created
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}

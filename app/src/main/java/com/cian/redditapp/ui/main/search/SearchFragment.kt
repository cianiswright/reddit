package com.cian.redditapp.ui.main.search

import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.cian.domain.model.History
import com.cian.domain.model.Suggestions
import com.cian.domain.source.state.DataState
import com.cian.redditapp.R
import com.cian.redditapp.databinding.FragmentSearchBinding
import com.cian.redditapp.extensions.*
import com.cian.redditapp.injection.module.feature.SuggestionComponent
import com.cian.redditapp.injection.module.feature.SuggestionModule
import com.cian.redditapp.ui.main.search.adapter.HistoryAdapter
import com.cian.redditapp.ui.main.search.adapter.SuggestionAdapter

class SearchFragment : Fragment(R.layout.fragment_search) {
    // Fragment
    private lateinit var component: SuggestionComponent
    private val model: SuggestionViewModel by lazy { getViewModel { component.suggestionViewModel } }
    private val binding by viewBinding(FragmentSearchBinding::bind)
    // Data
    private val suggestionAdapter get() = binding.rowSuggestions.adapter as SuggestionAdapter
    private val historyAdapter get() = binding.rowHistory.adapter as HistoryAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.run { component = app.appComponent.plus(SuggestionModule()) }

        init()
        observers()
        onSearchListeners()
    }

    private fun init() {
        model.getHistory()
        binding.rowSuggestions.adapter = SuggestionAdapter { search -> saveHistory(search, true) }
        binding.rowHistory.adapter = HistoryAdapter(
            { history -> saveHistory(history.historyItem, history.isSub) },
            { history -> deleteHistory(history) })

        binding.rowSuggestions.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            if (!suggestionAdapter.hasObservers())
                suggestionAdapter.setHasStableIds(true)
            adapter = suggestionAdapter
        }

        binding.rowHistory.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            if (!historyAdapter.hasObservers())
                historyAdapter.setHasStableIds(true)
            adapter = historyAdapter
        }
    }

    private fun observers() {
        model.suggestionsLiveData.observe(viewLifecycleOwner, { searchState(it) })
        model.historyLiveData.observe(viewLifecycleOwner, { historyAdapter.addItems(it.reversed()) })
    }

    private fun onSearchListeners() {
        binding.includeSearch.etSearch.addTextChangedListener(object : TextWatcher {
            var timer: CountDownTimer? = null

            override fun afterTextChanged(search: Editable?) {
                // Wait for user to stop typing
                timer?.cancel()
                timer = object : CountDownTimer(500, 1000) {
                    override fun onTick(millisUntilFinished: Long) {}
                    override fun onFinish() {
                        if (search!!.isNotEmpty()) showSuggestions(true)
                        else showSuggestions(false)
                        requestSuggestions(search.toString())
                    }
                }.start()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (count != 0) binding.includeSearch.ivCancel.visible()
                else binding.includeSearch.ivCancel.gone()
            }
        })

        // Done key listener for keyboard
        binding.includeSearch.etSearch.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE)
                saveHistory(view.text.toString(), false)
            false
        }

        // Search cancel listener
        binding.includeSearch.ivCancel.onClick {
            clearSearch()
            requireView().hideKeyboard()
        }
    }

    private fun searchState(dataState: DataState?) {
        when (dataState) {
            is DataState.Error -> showSuggestions(false)

            is DataState.Success<*> -> {
                if (!(dataState.data as Suggestions).suggestions.isNullOrEmpty())
                    showSuggestions(true)
                else showSuggestions(false)

                suggestionAdapter.updateItems((dataState.data as Suggestions).suggestions)
            }
        }
    }

    private fun showSuggestions(shouldShow: Boolean) {
        if (shouldShow) {
            binding.rowSuggestions.visible()
            binding.rowHistory.gone()
        } else {
            binding.rowSuggestions.gone()
            binding.rowHistory.visible()
        }
    }

    private fun clearSearch() {
        binding.includeSearch.etSearch.text.clear()
        binding.includeSearch.ivCancel.gone()
    }

    private fun saveHistory(search: String, isSub: Boolean) {
        model.addHistory(search, isSub)
        navigateWithArgs(search)
    }

    private fun deleteHistory(history: History) {
        model.deleteHistory(history)
        historyAdapter.removeItem(history)
    }

    private fun requestSuggestions(search: String) = model.requestSuggestions(search, "on")

    private fun navigateWithArgs(search: String) {
        view?.hideKeyboard()
        findNavController().navigate(SearchFragmentDirections.navigateSearchToMain(search))
    }
}
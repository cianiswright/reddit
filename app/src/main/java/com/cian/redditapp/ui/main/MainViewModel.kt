package com.cian.redditapp.ui.main

import androidx.lifecycle.ViewModel
import com.cian.domain.usecase.GetThemeUseCase
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getThemeUseCase: GetThemeUseCase
) : ViewModel() {

    fun getTheme() = getThemeUseCase.execute()
}
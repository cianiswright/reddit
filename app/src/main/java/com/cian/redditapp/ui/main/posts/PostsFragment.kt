package com.cian.redditapp.ui.main.posts

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.cian.domain.model.Posts
import com.cian.domain.source.state.DataState
import com.cian.domain.util.Constants.BASE_URL
import com.cian.redditapp.BuildConfig
import com.cian.redditapp.R
import com.cian.redditapp.common.InfiniteScrollListener
import com.cian.redditapp.common.snackbar
import com.cian.redditapp.databinding.FragmentPostsBinding
import com.cian.redditapp.extensions.*
import com.cian.redditapp.injection.module.feature.PostsComponent
import com.cian.redditapp.injection.module.feature.PostsModule
import com.cian.redditapp.ui.main.posts.adapter.PostsAdapter
import com.google.android.material.bottomsheet.BottomSheetDialog

class PostsFragment : Fragment(R.layout.fragment_posts) {
    // Fragment
    private lateinit var component: PostsComponent
    private val model: PostsViewModel by lazy { getViewModel { component.postsViewModel } }
    private val binding by viewBinding(FragmentPostsBinding::bind)
    // Data
    private val postAdapter get() = binding.postsContent.rowPosts.adapter as PostsAdapter
    private var postsData: Posts? = null
    private val args: PostsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.run { component = app.appComponent.plus(PostsModule()) }

        if (args.searchSub.isNotEmpty()) // Search args passed from navigation
            requestData(args.searchSub, model.currentFilter)
        else if (postsData == null) // If there's no data, request default
            requestData("popular", "best")

        init()
        observers()
        clickListeners()
        onBackPressed()
    }

    private fun init() {
        binding.postsContent.viewModel = model
        binding.postsContent.rowPosts.adapter = PostsAdapter { url -> redirect(url) }
        val build = resources.getString(R.string.build) + " " + BuildConfig.VERSION_NAME
        binding.postsMenu.tvBuild.text = build

        val linearLayout = LinearLayoutManager(context)
        binding.postsContent.rowPosts.apply {
            setHasFixedSize(true)
            layoutManager = linearLayout
            clearOnScrollListeners()
            addOnScrollListener(
                InfiniteScrollListener(
                    { requestData(model.currentRequest, model.currentFilter) },
                    linearLayout, binding.postsContent.fabSearch
                )
            )
            if (!postAdapter.hasObservers())
                postAdapter.setHasStableIds(true)
            adapter = postAdapter
        }

        binding.postsContent.swipeRefreshLayout.setOnRefreshListener {
            binding.postsContent.swipeRefreshLayout.isRefreshing = false
            postsData = null
            model.postsLoading.value = true
            requestData(model.currentRequest, model.currentFilter)
        }
    }

    private fun observers() {
        model.postsLiveData.observe(viewLifecycleOwner) {
            model.postsLoading.value = false
            postsState(it)
        }
    }

    private fun clickListeners() {
        // menu
        binding.postsMenu.btnTheme.onClick { showBottomSheet("theme", null) }
        binding.postsMenu.btnClearHistory.onClick {
            model.clearHistory()
            binding.motionLayout.transitionToStart()
        }

        binding.postsContent.fabSearch.onClick { findNavController().navigate(R.id.navigate_to_search) }
        binding.postsContent.ivFilter.onClick { showBottomSheet("filter", binding.postsContent.ivFilter) }
        binding.postsContent.ivViewType.onClick { showBottomSheet("viewType", binding.postsContent.ivViewType) }
    }

    private fun postsState(dataState: DataState) {
        when (dataState) {
            is DataState.Error -> {
                snackbar(requireView(), dataState.message.orEmpty(), R.string.retry)
                { requestData(model.currentRequest, model.currentFilter) }
                updateView(false)
            }

            is DataState.Success<*> -> {
                postsData = dataState.data as Posts?

                if (!(dataState).newRequest!!)
                    postAdapter.addItems(postsData!!.posts)
                else postAdapter.clearAndAddItems(postsData!!.posts)

                updateView(true)
            }
        }
    }

    private fun updateView(success: Boolean) {
        when (success) {
            false -> {
                binding.postsContent.tvPrefixSubreddit.invisible()
                binding.postsContent.rowPosts.invisible()
            }

            true -> {
                val prefix = resources.getString(R.string.prefix) + model.currentRequest
                binding.postsContent.tvPrefixSubreddit.text = prefix
                binding.postsContent.tvPrefixSubreddit.visible()
                binding.postsContent.rowPosts.visible()
            }
        }
    }

    private fun showBottomSheet(type: String, imageView: ImageView?) {
        val bottomSheetDialog = BottomSheetDialog(requireContext())
        when (type) {
            "filter" -> {
                bottomSheetDialog.setContentView(R.layout.bottom_sheet_filter)
                when (model.currentFilter) {
                    "best" -> bottomSheetDialog.findViewById<ImageView>(R.id.ivCheckBest)?.visible()
                    "hot" -> bottomSheetDialog.findViewById<ImageView>(R.id.ivCheckHot)?.visible()
                    "rising" -> bottomSheetDialog.findViewById<ImageView>(R.id.ivCheckRising)
                        ?.visible()
                }
                handleFilterClick(bottomSheetDialog, R.id.layoutBest, imageView, "best")
                handleFilterClick(bottomSheetDialog, R.id.layoutHot, imageView, "hot")
                handleFilterClick(bottomSheetDialog, R.id.layoutRising, imageView, "rising")
            }
            "viewType" -> {
                bottomSheetDialog.setContentView(R.layout.bottom_sheet_view)
                when (model.currentViewType) {
                    "card" -> bottomSheetDialog.findViewById<ImageView>(R.id.ivCheckCard)?.visible()
                    "list" -> bottomSheetDialog.findViewById<ImageView>(R.id.ivCheckList)?.visible()
                }
                handleFilterClick(bottomSheetDialog, R.id.layoutCard, imageView, "card")
                handleFilterClick(bottomSheetDialog, R.id.layoutList, imageView, "list")
            }
            "theme" -> {
                bottomSheetDialog.setContentView(R.layout.bottom_sheet_theme)
                when (model.getTheme()) {
                    "red" -> bottomSheetDialog.findViewById<ImageView>(R.id.ivCheckRed)?.visible()
                    "green" -> bottomSheetDialog.findViewById<ImageView>(R.id.ivCheckGreen)?.visible()
                    "blue" -> bottomSheetDialog.findViewById<ImageView>(R.id.ivCheckBlue)?.visible()
                }
                handleFilterClick(bottomSheetDialog, R.id.layoutRed, imageView, "red")
                handleFilterClick(bottomSheetDialog, R.id.layoutGreen, imageView, "green")
                handleFilterClick(bottomSheetDialog, R.id.layoutBlue, imageView, "blue")
            }
        }

        bottomSheetDialog.show()
    }

    private fun handleFilterClick(
        bottomSheetDialog: BottomSheetDialog,
        layout: Int,
        imageView: ImageView?,
        filter: String
    ) {
        bottomSheetDialog.findViewById<View>(layout)?.onClick {
            when (filter) {
                "card" -> {
                    postAdapter.changeViewType(true)
                    model.currentViewType = filter
                }
                "list" -> {
                    postAdapter.changeViewType(false)
                    model.currentViewType = filter
                }
                "red" -> {
                    model.saveTheme("red")
                    requireActivity().recreate()
                }
                "green" -> {
                    model.saveTheme("green")
                    requireActivity().recreate()
                }
                "blue" -> {
                    model.saveTheme("blue")
                    requireActivity().recreate()
                }
                else -> requestData(model.currentRequest, filter)
            }

            imageView?.updateFilterImageIcon(filter)
            bottomSheetDialog.dismiss()
        }
    }

    private fun requestData(subreddit: String, filter: String) {
        if (subreddit != model.currentRequest || filter != model.currentFilter) {
            binding.postsContent.rowPosts.invisible()
            binding.postsContent.tvPrefixSubreddit.invisible()
        }

        model.requestPosts(subreddit, filter, postsData?.after.orEmpty())
    }

    private fun redirect(url: String?) =
        requireContext().startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(BASE_URL + url)))


    private fun onBackPressed() { // Scroll to top on back pressed or close app if at top
        activity?.onBackPressedDispatcher?.addCallback(
            viewLifecycleOwner, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if ((binding.postsContent.rowPosts.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition() != 0)
                        binding.postsContent.rowPosts.layoutManager?.scrollToPosition(0)
                    else requireActivity().finish()
                }
            })
    }
}
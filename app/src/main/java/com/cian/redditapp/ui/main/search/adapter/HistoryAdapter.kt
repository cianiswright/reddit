package com.cian.redditapp.ui.main.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cian.redditapp.R
import com.cian.domain.model.History
import com.cian.redditapp.databinding.ItemHistoryBinding
import com.cian.redditapp.extensions.onClick

class HistoryAdapter(val listener: (History) -> Unit, val delete: (History) -> Unit) :
    RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

    var historyItems: ArrayList<History> = ArrayList()

    override fun getItemCount(): Int = historyItems.size

    override fun getItemId(position: Int): Long = position.toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val binding = DataBindingUtil.inflate<ItemHistoryBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_history, parent, false)
        return HistoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.bind(historyItems[position])
    }

    inner class HistoryViewHolder(internal val binding: ItemHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.onClick { listener(binding.history!!) }
            binding.ivCancelHistory.onClick { delete(binding.history!!) }
        }

        fun bind(history: History) { binding.history = history }
    }

    fun addItems(history: List<History>) {
        historyItems.clear()
        historyItems.addAll(history)
        notifyDataSetChanged()
    }

    fun removeItem(history: History) {
        historyItems.remove(history)
        notifyDataSetChanged()
    }
}
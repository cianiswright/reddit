package com.cian.redditapp.ui.main.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cian.domain.model.History
import com.cian.domain.source.state.DataState
import com.cian.domain.usecase.DeleteHistoryUseCase
import com.cian.domain.usecase.GetHistoryUseCase
import com.cian.domain.usecase.GetSuggestionsUseCase
import com.cian.domain.usecase.SaveHistoryUseCase
import com.cian.redditapp.common.BaseViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class SuggestionViewModel @Inject constructor(
    private val getSuggestionsUseCase: GetSuggestionsUseCase,
    private val getHistoryUseCase: GetHistoryUseCase,
    private val saveHistoryUseCase: SaveHistoryUseCase,
    private val deleteHistoryUseCase: DeleteHistoryUseCase,
    dispatcher: CoroutineDispatcher
) : BaseViewModel(dispatcher) {

    private val searchState = MutableLiveData<DataState>()
    val suggestionsLiveData: LiveData<DataState>
        get() = searchState

    private val historyList = MutableLiveData<ArrayList<History>>()
    val historyLiveData: LiveData<ArrayList<History>>
        get() = historyList

    fun requestSuggestions(query: String, flag: String) = launch {
        launch(Dispatchers.IO) {
            searchState.postValue(getSuggestionsUseCase.execute(query, flag).data)
        }
    }

    // History room Db
    fun getHistory() = launch {
        historyList.postValue(getHistoryUseCase.execute() as ArrayList<History>)
    }

    fun addHistory(history: String, isSub: Boolean) = launch {
        saveHistoryUseCase.execute(History(0, history, isSub))
    }

    fun deleteHistory(history: History) = launch {
        deleteHistoryUseCase.execute(history)
        historyList.value!!.remove(history)
    }
}
package com.cian.redditapp.ui.main.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.cian.redditapp.R
import com.cian.domain.model.SuggestionItem
import com.cian.redditapp.databinding.ItemSuggestionBinding
import com.cian.redditapp.extensions.loadSuggestionImage
import com.cian.redditapp.extensions.onClick
import com.cian.redditapp.ui.main.search.SuggestionDiffUtil

class SuggestionAdapter(val listener: (String) -> Unit) :
    RecyclerView.Adapter<SuggestionAdapter.SuggestionViewHolder>() {

    private var suggestions = ArrayList<SuggestionItem>()

    override fun getItemCount(): Int = suggestions.size

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemViewType(position: Int): Int = position

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuggestionViewHolder {
        val binding = DataBindingUtil.inflate<ItemSuggestionBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_suggestion, parent, false
        )
        return SuggestionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SuggestionViewHolder, position: Int) {
        holder.bind(suggestions[position])
    }

    fun updateItems(newSuggestions: List<SuggestionItem>) {
        val diffCallback = SuggestionDiffUtil(suggestions, newSuggestions)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        suggestions.clear()
        suggestions.addAll(newSuggestions)
        diffResult.dispatchUpdatesTo(this)
        notifyDataSetChanged()
    }

    inner class SuggestionViewHolder(internal val binding: ItemSuggestionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.onClick { listener(binding.suggestion!!.sub!!) }
        }

        fun bind(search: SuggestionItem) {
            binding.suggestion = search

            val iconUrl = if (!search.iconImg.isNullOrEmpty()) search.iconImg
            else if (!search.iconCommunity.isNullOrEmpty()) search.iconCommunity!!.split("?")[0]
            else ""
            binding.ivSubreddit.loadSuggestionImage(iconUrl!!)
        }
    }
}
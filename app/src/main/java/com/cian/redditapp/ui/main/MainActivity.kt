package com.cian.redditapp.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cian.redditapp.R
import com.cian.redditapp.extensions.app
import com.cian.redditapp.extensions.getViewModel
import com.cian.redditapp.injection.RedditApplication
import com.cian.redditapp.injection.module.feature.ThemeComponent
import com.cian.redditapp.injection.module.feature.ThemeModule

class MainActivity : AppCompatActivity() {

    private lateinit var component: ThemeComponent
    private val model: MainViewModel by lazy { getViewModel { component.mainViewModel } }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RedditApplication.instance.appComponent.inject(this)
        component = app.appComponent.plus(ThemeModule())

        when (model.getTheme()) {
            "red" -> setTheme(R.style.RedAccent)
            "green" -> setTheme(R.style.GreenAccent)
            "blue" -> setTheme(R.style.BlueAccent)
        }

        setContentView(R.layout.activity_posts)
    }
}

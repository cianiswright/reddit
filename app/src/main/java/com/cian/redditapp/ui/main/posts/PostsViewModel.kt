package com.cian.redditapp.ui.main.posts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cian.domain.source.state.DataState
import com.cian.domain.usecase.DeleteAllHistoryUseCase
import com.cian.domain.usecase.GetPostsUseCase
import com.cian.domain.usecase.GetThemeUseCase
import com.cian.domain.usecase.SaveThemeUseCase
import com.cian.redditapp.common.BaseViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class PostsViewModel @Inject constructor(
    private val getPostsUseCase: GetPostsUseCase,
    private val getThemeUseCase: GetThemeUseCase,
    private val saveThemeUseCase: SaveThemeUseCase,
    private val deleteAllHistoryUseCase: DeleteAllHistoryUseCase,
    dispatcher: CoroutineDispatcher
) : BaseViewModel(dispatcher) {

    private val postsState = MutableLiveData<DataState>()
    val postsLiveData: LiveData<DataState>
        get() = postsState

    var currentRequest: String = ""
    var currentFilter: String = "best" // default filter
    var currentViewType: String = "card"

    val postsLoading = MutableLiveData<Boolean>()

    fun requestPosts(subreddit: String, filter: String, after: String?) = launch {
        val isNewRequest = subreddit != currentRequest || filter != currentFilter
        postsLoading.value = isNewRequest

        launch(Dispatchers.IO) {
            val result = getPostsUseCase.execute(subreddit, filter, after, isNewRequest)

            // Set values for additional requests
            currentRequest = subreddit
            currentFilter = filter
            postsState.postValue(result.data)
        }
    }

    fun getTheme() = getThemeUseCase.execute()

    fun saveTheme(theme: String) = launch { saveThemeUseCase.execute(theme) }

    fun clearHistory() = launch { deleteAllHistoryUseCase.execute() }
}
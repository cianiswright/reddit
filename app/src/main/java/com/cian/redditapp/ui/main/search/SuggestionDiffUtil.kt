package com.cian.redditapp.ui.main.search

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.cian.domain.model.SuggestionItem

class SuggestionDiffUtil(
    private val oldList: List<SuggestionItem>,
    private val newList: List<SuggestionItem>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].sub == newList[newItemPosition].sub

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem.sub == newItem.sub &&
                oldItem.subscribers == newItem.subscribers &&
                oldItem.iconCommunity == newItem.iconCommunity &&
                oldItem.iconImg == newItem.iconImg
    }

    @Nullable
    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }
}
package com.cian.redditapp.common

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class InfiniteScrollListener(
    val func: () -> Unit,
    private val layoutManager: LinearLayoutManager,
    private val searchBtn: FloatingActionButton
) : RecyclerView.OnScrollListener() {
    private var loading = true

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            searchBtn.show()

            val lastVisibleItemPosition = (recyclerView.layoutManager
                    as LinearLayoutManager).findLastCompletelyVisibleItemPosition()

            loading = if (!loading && layoutManager.itemCount == lastVisibleItemPosition + 1) {
                func()
                true
            } else
                false
        } else
            searchBtn.hide()
    }
}
package com.cian.redditapp.common

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineDispatcher

abstract class BaseViewModel(dispatcher: CoroutineDispatcher) : ViewModel(),
Scope by Scope.Implement(dispatcher){

    init {
        initScope()
    }

    @CallSuper
    override fun onCleared() {
        cancelScope()
        super.onCleared()
    }
}
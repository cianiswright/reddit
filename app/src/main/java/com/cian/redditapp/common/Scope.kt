package com.cian.redditapp.common

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

interface Scope : CoroutineScope {

    var job: Job
    val dispatcher: CoroutineDispatcher

    class Implement(override val dispatcher: CoroutineDispatcher) : Scope {
        override lateinit var job: Job
    }

    override val coroutineContext: CoroutineContext
        get() = dispatcher + job

    fun initScope() { job = SupervisorJob() }

    fun cancelScope() = coroutineContext.cancelChildren()
}
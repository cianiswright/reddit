package com.cian.redditapp.common

interface ViewType {
    fun getViewType(): Int
}
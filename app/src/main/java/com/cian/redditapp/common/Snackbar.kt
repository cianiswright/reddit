package com.cian.redditapp.common

import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar

fun snackbar(rootView: View, message: String, @StringRes action: Int, func: () -> Unit) =
    Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
        .setAction(action) { func() }
        .show()
package com.cian.redditapp

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.cian.data.util.ResourceProvider
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

private const val REQUEST_RESPONSE = "REQUEST ERROR"
private const val SUB_RESPONSE = "SUB ERROR"

@RunWith(MockitoJUnitRunner::class)
class ResourceHelperTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock private lateinit var mockContext: Context
    @Mock private lateinit var resourceProvider: ResourceProvider

    @Test
    fun readResources() {
        `when`(mockContext.getString(R.string.server_error)).thenReturn(REQUEST_RESPONSE)
        `when`(mockContext.getString(R.string.subreddit_not_found)).thenReturn(SUB_RESPONSE)

        resourceProvider = ResourceProvider(mockContext)
        val requestError = resourceProvider.serverError
        val subError = resourceProvider.subredditNotFound

        assertThat(requestError, `is`(REQUEST_RESPONSE))
        assertThat(subError, `is`(SUB_RESPONSE))
    }
}
package com.cian.redditapp.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result
import com.cian.domain.usecase.DeleteAllHistoryUseCase
import com.cian.domain.usecase.GetPostsUseCase
import com.cian.domain.usecase.GetThemeUseCase
import com.cian.domain.usecase.SaveThemeUseCase
import com.cian.redditapp.errorMessage
import com.cian.redditapp.postsDataStateError
import com.cian.redditapp.postsDataStateSuccess
import com.cian.redditapp.ui.main.posts.PostsViewModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class PostsViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private var getPostsUseCase = mock<GetPostsUseCase>()
    private var getThemeUseCase = mock<GetThemeUseCase>()
    private var saveThemeUseCase = mock<SaveThemeUseCase>()
    private var deleteAllHistoryUseCase = mock<DeleteAllHistoryUseCase>()
    private var viewModel = mock<PostsViewModel>()
    private var result = mock<Result<DataState>>()

    @Before
    fun setUp() {
        viewModel = PostsViewModel(
            getPostsUseCase,
            getThemeUseCase,
            saveThemeUseCase,
            deleteAllHistoryUseCase,
            Dispatchers.Unconfined
        )
    }

    @Test
    fun `When requesting posts data, a successful result and data state is returned`() {
        runBlocking {
            result = Result.success(postsDataStateSuccess)

            whenever(getPostsUseCase.execute("", "", "", true))
                .thenReturn(result)

            viewModel.requestPosts("", "", "")
            verify(getPostsUseCase).execute("", "", "", true)
        }

        assertEquals(viewModel.postsLiveData.value, result.data)
    }

    @Test
    fun `When requesting posts data, an error is returned`() {
        runBlocking {
            result = Result.error(errorMessage, postsDataStateError)

            whenever(getPostsUseCase.execute("abcd", "", "", true))
                .thenReturn(result)

            viewModel.requestPosts("abcd", "", "")
            verify(getPostsUseCase).execute("abcd", "", "", true)
        }
        assertEquals(viewModel.postsLiveData.value, result.data)
    }
}
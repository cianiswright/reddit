package com.cian.redditapp

import com.cian.domain.model.PostItem
import com.cian.domain.model.Posts
import com.cian.domain.model.SuggestionItem
import com.cian.domain.model.Suggestions
import com.cian.domain.source.state.DataState

// Messages
const val errorMessage = "Error message"

// Posts data
val postItem = PostItem("subreddit", "author", "title", 0, 0, 0, "media_type", "media_url", "thumbnail", "link")
val posts = Posts(listOf(postItem), "after")

// Suggestion data
val suggestionItem = SuggestionItem("subreddit", 0, "icon", "image")
val suggestions = Suggestions(listOf(suggestionItem))

// Data state
val postsDataStateSuccess = DataState.Success(posts, true)
val postsDataStateError = DataState.Error(errorMessage)
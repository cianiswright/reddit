package com.cian.data.model.mapper

import com.cian.data.model.response.PostChildrenResponse
import com.cian.domain.model.PostItem

val mapServerPostsToDomain: (PostChildrenResponse) -> PostItem = { postsResponse ->
    val item = postsResponse.data
    PostItem(
        subreddit = item.subreddit_name_prefixed,
        user = item.author,
        title = item.title,
        comments = item.num_comments,
        created = item.created,
        votes = item.ups,
        mediaType = item.post_hint,
        media = item.url_overridden_by_dest,
        thumbnail = item.thumbnail,
        url = item.permalink
    )
}
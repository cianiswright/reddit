package com.cian.data.model.response

class PostsResponse(val data: PostsDataResponse)

class PostsDataResponse(
    val children: List<PostChildrenResponse>,
    val after: String?
)

class PostChildrenResponse(val data: PostChildDataResponse)

class PostChildDataResponse(
    val subreddit_name_prefixed: String,
    val author: String,
    val title: String,
    val num_comments: Int,
    val created: Long,
    val ups: Int,
    val post_hint: String?,
    val url_overridden_by_dest: String?,
    val thumbnail: String?,
    val permalink: String
)
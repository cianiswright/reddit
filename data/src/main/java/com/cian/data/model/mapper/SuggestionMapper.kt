package com.cian.data.model.mapper

import com.cian.data.model.response.SuggestionChildrenResponse
import com.cian.domain.model.SuggestionItem

val mapServerSuggestionsToDomain: (SuggestionChildrenResponse) -> SuggestionItem = { suggestionResponse ->
    val item = suggestionResponse.data

    item.display_name.isNotEmpty() // omit user responses
    SuggestionItem(
        item.display_name,
        item.subscribers,
        item.community_icon,
        item.icon_img
    )
}
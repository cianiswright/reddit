package com.cian.data.model.response

class SuggestionResponse(val data: SuggestionDataResponse)

class SuggestionDataResponse(val children: List<SuggestionChildrenResponse>)

class SuggestionChildrenResponse(val data: SuggestionChildDataResponse)

class SuggestionChildDataResponse(
    val display_name: String,
    val subscribers: Int,
    val community_icon: String,
    val icon_img: String
)
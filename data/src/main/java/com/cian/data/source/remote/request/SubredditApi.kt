package com.cian.data.source.remote.request

import com.cian.data.model.response.PostsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SubredditApi {
    @GET("/r/{subreddit}/{filter}.json")
    suspend fun getSubredditData(
        @Path("subreddit") subreddit: String,
        @Path("filter") filter: String,
        @Query("after") after: String?
    ): Response<PostsResponse>
}
package com.cian.data.source.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.OnConflictStrategy
import com.cian.domain.model.History

@Dao
interface HistoryDao {
    @Query("SELECT * FROM history")
    suspend fun loadAllHistory(): List<History>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveHistory(history: History)

    @Delete
    suspend fun deleteHistory(history: History)

    @Query("DELETE FROM history")
    suspend fun deleteAllHistory()

    @Query("SELECT COUNT(historyItem) FROM history WHERE historyItem = :historyItem")
    suspend fun doesHistoryExist(historyItem: String): Int
}
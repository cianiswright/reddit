package com.cian.data.source.remote.request

import com.cian.data.model.response.SuggestionResponse
import retrofit2.Response
import retrofit2.http.*

interface SuggestionApi {
    @GET("/api/subreddit_autocomplete_v2.json")
    suspend fun getSuggestionData(
        @Query("query") search: String,
        @Query("include_over_18") state: String
    ): Response<SuggestionResponse>
}
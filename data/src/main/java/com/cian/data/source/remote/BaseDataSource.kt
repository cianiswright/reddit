package com.cian.data.source.remote

import com.cian.domain.source.state.Result
import com.cian.domain.util.Constants
import com.cian.domain.util.Constants.Status.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.text.ParseException

abstract class BaseDataSource<Dto, DomainModel> {
    protected suspend fun getResult(
        call: suspend () -> Response<Dto>
    ): Result<DomainModel> = try {
        withContext(Dispatchers.IO) {
            Result.success(getDataState(SUCCESS, call().body()!!))
        }
    } catch (time: TimeoutCancellationException) {
        Result.error(time.message ?: time.toString(), getDataState(TIME_OUT_ERROR, null), isTimeout = true)
    } catch (pe: ParseException) {
        Result.error(pe.message.toString(), getDataState(PARSE_ERROR, null), isParseError = true)
    } catch (e: Exception) {
        Result.error(e.message.toString(), getDataState(ERROR, null))
    }

    protected abstract fun getDataState(status: Constants.Status, dtoObject: Dto?): DomainModel

    protected abstract fun mapResultToDomainModel(dtoObject: Dto): List<*>
}
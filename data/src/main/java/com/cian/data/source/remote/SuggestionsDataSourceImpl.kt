package com.cian.data.source.remote

import com.cian.data.model.mapper.mapServerSuggestionsToDomain
import com.cian.data.model.response.SuggestionResponse
import com.cian.data.source.remote.request.SuggestionApi
import com.cian.data.util.ResourceProvider
import com.cian.domain.model.SuggestionItem
import com.cian.domain.model.Suggestions
import com.cian.domain.source.SuggestionsDataSource
import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result
import com.cian.domain.util.Constants
import com.cian.domain.util.Constants.Status.*
import kotlinx.coroutines.withTimeout

class SuggestionsDataSourceImpl(
    private val suggestionApi: SuggestionApi,
    private val resourceProvider: ResourceProvider
) : BaseDataSource<SuggestionResponse, DataState>(), SuggestionsDataSource {

    override suspend fun getDataSource(query: String, flag: String): Result<DataState> =
        getResult {
            withTimeout(8100L) {
                suggestionApi.getSuggestionData(query, flag)
            }
        }

    override fun getDataState(status: Constants.Status, dtoObject: SuggestionResponse?): DataState {
        return when (status) {
            SUCCESS -> {
                val data = mapResultToDomainModel(dtoObject!!)
                if (data.isNullOrEmpty())
                    DataState.Error(resourceProvider.noMatchingSuggestions)
                else DataState.Success(Suggestions(mapResultToDomainModel(dtoObject)), null)
            }
            ERROR -> DataState.Error(resourceProvider.serverError)
            TIME_OUT_ERROR -> DataState.Error(resourceProvider.timeOut)
            PARSE_ERROR -> DataState.Error(resourceProvider.parseError)
        }
    }

    override fun mapResultToDomainModel(dtoObject: SuggestionResponse): List<SuggestionItem> =
        dtoObject.data.children.map(mapServerSuggestionsToDomain)
}
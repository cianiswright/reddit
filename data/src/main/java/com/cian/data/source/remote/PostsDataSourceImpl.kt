package com.cian.data.source.remote

import com.cian.data.model.mapper.mapServerPostsToDomain
import com.cian.data.model.response.PostsResponse
import com.cian.data.source.remote.request.SubredditApi
import com.cian.data.util.ResourceProvider
import com.cian.domain.model.PostItem
import com.cian.domain.model.Posts
import com.cian.domain.source.PostsDataSource
import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result
import com.cian.domain.util.Constants
import com.cian.domain.util.Constants.Status.*
import kotlinx.coroutines.withTimeout

class PostsDataSourceImpl(
    private val subredditApi: SubredditApi,
    private val resourceProvider: ResourceProvider
) : BaseDataSource<PostsResponse, DataState>(), PostsDataSource {

    private var newRequest: Boolean = false

    override suspend fun getDataSource(
        subreddit: String,
        filter: String,
        after: String?,
        isNewRequest: Boolean
    ): Result<DataState> =
        getResult {
            withTimeout(8100L) {
                newRequest = isNewRequest
                subredditApi.getSubredditData(subreddit, filter, after)
            }
        }

    override fun getDataState(status: Constants.Status, dtoObject: PostsResponse?): DataState {
        return when (status) {
            SUCCESS -> {
                val data = mapResultToDomainModel(dtoObject!!)
                if (data.isNullOrEmpty())
                    DataState.Error(resourceProvider.subredditNotFound)
                else {
                    DataState.Success(Posts(mapResultToDomainModel(dtoObject), dtoObject.data.after.orEmpty()), newRequest)
                }
            }
            ERROR -> DataState.Error(resourceProvider.serverError)
            TIME_OUT_ERROR -> DataState.Error(resourceProvider.timeOut)
            PARSE_ERROR -> DataState.Error(resourceProvider.parseError)
        }
    }

    override fun mapResultToDomainModel(dtoObject: PostsResponse): List<PostItem> =
        dtoObject.data.children.map(mapServerPostsToDomain)
}
package com.cian.data.util

import org.joda.time.*
import java.util.*

class FormatTime {
    companion object {
        fun getTime(time: Long): String {
            val dateTime = DateTime(time * 1000)
            val current = DateTime(Calendar.getInstance().time)
            val created: String

            val seconds = Seconds.secondsBetween(dateTime, current).seconds
            val minutes = Minutes.minutesBetween(dateTime, current).minutes
            val hours = Hours.hoursBetween(dateTime, current).hours
            val days = Days.daysBetween(dateTime, current).days
            val months = Months.monthsBetween(dateTime, current).months
            val years = Years.yearsBetween(dateTime, current).years

            created = when {
                years > 0 -> if (years == 1) "a year ago" else "$years years ago"
                months > 0 -> if (months == 1) "a month ago" else "$months months ago"
                days > 0 -> if (days == 1) "a day ago" else "$days days ago"
                hours > 0 -> if (hours == 1) "an hour ago" else "$hours hours ago"
                minutes > 0 -> if (minutes == 1) "a minute ago" else "$minutes minutes ago"
                else -> if (seconds <= 1) "just now" else "$seconds seconds ago"
            }
            return created
        }
    }
}
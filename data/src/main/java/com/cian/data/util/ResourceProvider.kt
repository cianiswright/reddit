package com.cian.data.util

import android.content.Context
import com.cian.data.R

class ResourceProvider(private val context: Context) {
    // Response errors
    val subredditNotFound get() = context.getString(R.string.subreddit_not_found)
    val noMatchingSuggestions get() = context.getString(R.string.no_matched_suggestions)

    // Request errors
    val serverError get() = context.getString(R.string.server_error)
    val timeOut get() = context.getString(R.string.time_out)
    val parseError get() = context.getString(R.string.parse_error)
}
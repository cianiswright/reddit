package com.cian.data.repository

import com.cian.data.source.local.dao.HistoryDao
import com.cian.domain.model.History
import com.cian.domain.repository.HistoryRepository

class HistoryRepositoryImpl(
    private val dao: HistoryDao
) : HistoryRepository {

    override suspend fun getHistoryList(): List<History> = dao.loadAllHistory()

    override suspend fun addHistory(history: History) {
        if (doesHistoryExist(history.historyItem) == 0)
            dao.saveHistory(history) // save if the item doesn't exist
    }

    override suspend fun deleteHistory(history: History) = dao.deleteHistory(history)

    override suspend fun deleteAllHistory() = dao.deleteAllHistory()

    private suspend fun doesHistoryExist(historyItem: String) = dao.doesHistoryExist(historyItem)
}
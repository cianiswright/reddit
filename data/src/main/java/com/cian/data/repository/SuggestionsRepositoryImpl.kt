package com.cian.data.repository

import com.cian.domain.repository.SuggestionsRepository
import com.cian.domain.source.SuggestionsDataSource
import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result

class SuggestionsRepositoryImpl (
    private val suggestionsDataSource: SuggestionsDataSource
) : SuggestionsRepository {

    override suspend fun getSuggestions(query: String, flag: String): Result<DataState> =
        suggestionsDataSource.getDataSource(query, flag)
}
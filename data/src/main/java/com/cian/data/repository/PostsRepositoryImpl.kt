package com.cian.data.repository

import com.cian.domain.repository.PostsRepository
import com.cian.domain.source.PostsDataSource
import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result


class PostsRepositoryImpl(
    private val postsDataSource: PostsDataSource
) : PostsRepository {

    override suspend fun getPosts(
        subreddit: String,
        filter: String,
        after: String?,
        isNewRequest: Boolean
    ): Result<DataState> =
        postsDataSource.getDataSource(subreddit, filter, after, isNewRequest)
}
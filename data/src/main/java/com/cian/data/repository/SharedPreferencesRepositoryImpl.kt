package com.cian.data.repository

import android.content.SharedPreferences
import com.cian.domain.repository.SharedPreferencesRepository

class SharedPreferencesRepositoryImpl(
    private val prefs: SharedPreferences
) : SharedPreferencesRepository {

    override fun getTheme(): String? = prefs.getString("preference_theme", "Red")

    override fun saveTheme(theme: String) = prefs.edit().putString("preference_theme", theme).apply()
}



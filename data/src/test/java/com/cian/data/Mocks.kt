package com.cian.data

import com.cian.data.model.response.*
import com.cian.domain.model.PostItem
import com.cian.domain.model.Posts

// Posts response
val postsChildDataResponse = PostChildDataResponse("", "", "", 0, 0, 0, "", "", "", "")
val postsChildrenResponse = PostChildrenResponse(postsChildDataResponse)
val postsDataResponse = PostsDataResponse(listOf(postsChildrenResponse), "")
val postsResponse = PostsResponse(postsDataResponse)

// Suggestions response
val suggestionChildDataResponse = SuggestionChildDataResponse("", 0, "", "")
val suggestionChildrenResponse = SuggestionChildrenResponse(suggestionChildDataResponse)
val suggestionsDataResponse = SuggestionDataResponse(listOf(suggestionChildrenResponse))
val suggestionResponse = SuggestionResponse(suggestionsDataResponse)
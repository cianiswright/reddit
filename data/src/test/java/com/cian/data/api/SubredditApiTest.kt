package com.cian.data.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.cian.data.model.response.PostsResponse
import com.cian.data.postsResponse
import com.cian.data.source.remote.request.SubredditApi
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class SubredditApiTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private var apiResponse =  mock<Response<PostsResponse>>()
    private var service = mock<SubredditApi>()

    @Before
    fun setUp() {
        apiResponse = Response.success(postsResponse)
    }

    @Test
    fun `Return posts response from subreddit api`() {
        runBlocking {
            whenever(service.getSubredditData("", "", ""))
                .thenReturn(apiResponse)

            assertEquals(service.getSubredditData("", "", ""), apiResponse)
            verify(service).getSubredditData("", "", "")
        }
    }
}
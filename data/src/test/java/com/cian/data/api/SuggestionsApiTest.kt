package com.cian.data.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.cian.data.model.response.SuggestionResponse
import com.cian.data.source.remote.request.SuggestionApi
import com.cian.data.suggestionResponse
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class SuggestionsApiTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private var apiResponse = mock<Response<SuggestionResponse>>()
    private var service = mock<SuggestionApi>()

    @Before
    fun setUp() {
        apiResponse = Response.success(suggestionResponse)
    }

    @Test
    fun `Return posts response from subreddit api`() {
        runBlocking {
            whenever(service.getSuggestionData("", ""))
                .thenReturn(apiResponse)

            assertEquals(service.getSuggestionData("", ""), apiResponse)
            verify(service).getSuggestionData("", "")
        }
    }
}
package com.cian.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class History (
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "historyItem") var historyItem: String,
    @ColumnInfo(name = "isSub") var isSub: Boolean
)
package com.cian.domain.model

import com.squareup.moshi.Json

class Suggestions(val suggestions: List<SuggestionItem>)

class SuggestionItem(
    @Json(name = "display_name")
    var sub: String? = null,
    @Json(name = "subscribers")
    var subscribers: Int? = null,
    @Json(name = "community_icon")
    var iconCommunity: String? = null,
    @Json(name = "icon_img")
    var iconImg: String? = null
)
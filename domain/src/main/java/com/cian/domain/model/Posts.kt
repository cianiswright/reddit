package com.cian.domain.model

import com.squareup.moshi.Json

data class Posts(
    val posts: List<PostItem>,
    val after: String
)

data class PostItem(
    @Json(name = "subreddit_name_prefixed")
    val subreddit: String,
    @Json(name = "author")
    val user: String,
    @Json(name = "title")
    val title: String,
    @Json(name = "num_comments")
    val comments: Int,
    @Json(name = "created")
    val created: Long,
    @Json(name = "ups")
    val votes: Int,
    @Json(name = "post_hint")
    val mediaType: String?,
    @Json(name = "url_overridden_by_dest")
    val media: String?,
    @Json(name = "thumbnail")
    val thumbnail: String?,
    @Json(name = "permalink")
    val url: String
)
package com.cian.domain.repository

import com.cian.domain.model.History

interface HistoryRepository {

    suspend fun getHistoryList(): List<History>

    suspend fun addHistory(history: History)

    suspend fun deleteHistory(history: History)

    suspend fun deleteAllHistory()
}
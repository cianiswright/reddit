package com.cian.domain.repository

interface SharedPreferencesRepository {

    fun getTheme(): String?

    fun saveTheme(theme: String)
}



package com.cian.domain.repository

import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result

interface SuggestionsRepository {
    suspend fun getSuggestions(query: String, flag: String): Result<DataState>
}
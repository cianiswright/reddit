package com.cian.domain.repository

import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result

interface PostsRepository {
    suspend fun getPosts(subreddit: String, filter: String, after: String?, isNewRequest: Boolean): Result<DataState>
}
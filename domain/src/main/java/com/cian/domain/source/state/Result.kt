package com.cian.domain.source.state

import com.cian.domain.util.Constants

data class Result<out T>(
    val status: Constants.Status,
    val data: T? = null,
    val message: String?,
) {
    companion object {
        fun <T> success(data: T? = null): Result<T> {
            return Result(
                Constants.Status.SUCCESS,
                data,
                message = null
            )
        }

        fun <T> error(
            message: String,
            data: T? = null,
            isTimeout: Boolean = false,
            isParseError: Boolean = false
        ): Result<T> {
            return Result(
                when {
                    isTimeout -> Constants.Status.TIME_OUT_ERROR
                    isParseError -> Constants.Status.PARSE_ERROR
                    else -> Constants.Status.ERROR
                },
                data,
                message
            )
        }
    }
}
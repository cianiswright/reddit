package com.cian.domain.source

import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result

interface SuggestionsDataSource {
    suspend fun getDataSource(query: String, flag: String): Result<DataState>
}
package com.cian.domain.source.state

sealed class DataState {
    class Success<T>(val data: T, val newRequest: Boolean?) : DataState()
    class Error(val message: String?) : DataState()
}
package com.cian.domain.source

import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result

interface PostsDataSource {
    suspend fun getDataSource(subreddit: String, filter: String, after: String?, isNewRequest: Boolean): Result<DataState>
}
package com.cian.domain.util

object Constants {
    // URL
    const val BASE_URL = "https://www.reddit.com"

    // Adapter states
    const val LOADING = 1
    const val LISTVIEW = 2
    const val CARDVIEW = 3

    // Request status
    enum class Status {
        SUCCESS,
        ERROR,
        TIME_OUT_ERROR,
        PARSE_ERROR
    }
}
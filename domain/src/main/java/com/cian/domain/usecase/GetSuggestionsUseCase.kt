package com.cian.domain.usecase

import com.cian.domain.repository.SuggestionsRepository
import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result

class GetSuggestionsUseCase(
    private val suggestionsRepository: SuggestionsRepository
) {
    suspend fun execute(query: String, flag: String): Result<DataState> =
        suggestionsRepository.getSuggestions(query, flag)
}
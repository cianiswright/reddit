package com.cian.domain.usecase

import com.cian.domain.model.History
import com.cian.domain.repository.HistoryRepository

class SaveHistoryUseCase(
    private val historyRepository: HistoryRepository
) {
    suspend fun execute(history: History) = historyRepository.addHistory(history)
}
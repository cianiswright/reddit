package com.cian.domain.usecase

import com.cian.domain.model.History
import com.cian.domain.repository.HistoryRepository

class DeleteHistoryUseCase(
    private val historyRepository: HistoryRepository
) {
    suspend fun execute(history: History) = historyRepository.deleteHistory(history)
}
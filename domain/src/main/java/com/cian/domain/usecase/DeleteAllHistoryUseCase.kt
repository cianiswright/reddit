package com.cian.domain.usecase

import com.cian.domain.repository.HistoryRepository

class DeleteAllHistoryUseCase(
    private val historyRepository: HistoryRepository
) {
    suspend fun execute() = historyRepository.deleteAllHistory()
}
package com.cian.domain.usecase

import com.cian.domain.repository.SharedPreferencesRepository

class SaveThemeUseCase(
    private val sharedPreferencesRepository: SharedPreferencesRepository
) {
    fun execute(theme: String) = sharedPreferencesRepository.saveTheme(theme)
}
package com.cian.domain.usecase

import com.cian.domain.repository.SharedPreferencesRepository

class GetThemeUseCase(
    private val sharedPreferencesRepository: SharedPreferencesRepository
) {
    fun execute(): String? = sharedPreferencesRepository.getTheme()
}
package com.cian.domain.usecase

import com.cian.domain.repository.PostsRepository
import com.cian.domain.source.state.DataState
import com.cian.domain.source.state.Result

class GetPostsUseCase(
    private val postsRepository: PostsRepository
) {
    suspend fun execute(subreddit: String, filter: String, after: String?, isNewRequest: Boolean): Result<DataState> =
        postsRepository.getPosts(subreddit, filter, after, isNewRequest)
}
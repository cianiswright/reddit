package com.cian.domain.usecase

import com.cian.domain.model.History
import com.cian.domain.repository.HistoryRepository

class GetHistoryUseCase(
    private val historyRepository: HistoryRepository
) {
    suspend fun execute(): List<History> = historyRepository.getHistoryList()
}
# Reddit Reader
This is an Android app where you can search for, filter and scroll through subreddit posts.

[[_TOC_]]

## Project setup
Clone the repo, open the project in Android Studio and hit Run. It needs an active Internet connection to call Reddits APIs.

## About the app
Following MVVM with clean architecture pattern to separate presentation, business, and domain logic, this app makes use of Reddit APIs to allow you to search through posts, and redirect to the source when clicking. It also utilises Room and SharedPreferences locally for search history and custom theme changes respectfully.

It began as a small project that expanded in my free time to bring together libraries and implementations I've worked with, and learning a few new things along the way.

### General info
* Target api: 30
* Language: Kotlin
* Modular native app
* MVVM with clean code architecture
* Package structure inspired by features

### Features
* Extension binding to handle memory leaks
* Adapter reference from binding to avoid memory leaks
* Motion layout animations
* Endless scroll with swipe refresh
* Scroll to top on back pressed
* Filter subreddit by categories
* Change between card and list views
* Search for a subreddit which returns suggestions
* Local search history; delete and search again using the history
* Launch screen while the Main Activity is being created
* Option to change theme accent
* Supports images and gifs
* Bottom sheet dialog

### Libraries
* Kotlin coroutines, ViewModel, LiveData
* Data Binding, View Binding
* Room persistence, DiffUtil
* Retrofit 2, Moshi
* Dagger 2
* Koin implementation in separate branch (app version 4.1.5)
* Navigation component
* Kotlin extensions
* Glide

## External dependencies
The app makes use of two Reddit APIs:
* A search by subreddit and/or filter e.g. /r/{subreddit}/{filter}.json
* A suggestions query returning a short list of subreddits similar to the query parameter
